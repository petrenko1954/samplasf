#app_20
#Листинг 7.3: Добавление ресурса Users в файл маршрутов. config/routes.rb 
#Листинг 8.1: Добавление ресурса для получения стандартных RESTful действий для сессий. config/routes.rb 
SampleApp::Application.routes.draw do
#get "users/new"  
 root  'static_pages#home'

 resources :lectures


 get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'


resources :users
resources :users do
    member do
      get :following, :followers
    end
  end

resources :sessions, only: [:new, :create, :destroy]
resources :microposts, only: [:create, :destroy]
resources :relationships, only: [:create, :destroy]

 

 match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get'
  match '/contact', to: 'static_pages#contact', via: 'get'
match '/referat', to: 'static_pages#referat', via: 'get'
match '/july07', to: 'static_pages#july07', via: 'get'
match '/readme', to: 'static_pages#readme', via: 'get'



match '/tablecontent', to: 'static_pages#tablecontent', via: 'get'

#get "static_pages/tablecontent"
#get "static_pages/referat"
#get "static_pages/help"
#get "static_pages/July07"

  match '/signup',  to: 'users#new',            via: 'get'
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'
  end
