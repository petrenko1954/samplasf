class UsersController < ApplicationController
# get "users/new"
#Листинг 9.18.
#Листинг 9.34. Пагинация пользователей в index действии.
#Листинг 9.1: Действие edit.
#app/controllers/users_controller.rb 
#Листинг 9.33: Действие index. app/controllers/users_controller.rb  before_action :logged_in_user, 
#only: [:index, :edit, :update]
	#?  before_action :logged_in_user, only: [:index, :edit, :update]

before_action :signed_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]

before_action :admin_user,     only: :destroy

def index
@users = User.all
@users = User.paginate(page: params[:page])
  end
def edit
@user = User.find(params[:id])
  end

def show
    @user = User.find(params[:id])
 @microposts = @user.microposts.paginate(page: params[:page])
  end
def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted."
    redirect_to users_url
  end
  
def new
 @user = User.new
  end

def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

def create
    @user = User.new(user_params)
   if @user.save
log_in @user
flash[:success] = "добро пожаловать ! Welcome to the Oct12_Sample App!"
      redirect_to @user
    else
      render 'new'
    end
  end

#  def create
#    user = User.find_by(email: params[:session][:email].downcase)
#    if user && user.authenticate(params[:session][:password])
#      sign_in user
#      redirect_back_or user
#    else
#      flash.now[:error] = 'Invalid email/password combination'
#      render 'new'
#    end
#  end

 def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
 # Before filters

  
def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end



